class Asset extends HTMLElement {
    constructor() {
        super();
        this.image = null;
    }

    static get observedAttributes() { return ["image"]; }

    attributeChangedCallback(name, oldValue, newValue) {
        this.image = newValue;
        this.style.backgroundImage = `url(${newValue})`;
        this.updateRendering();
    }
    connectedCallback() {
        this.updateRendering();
    }

    updateRendering() {

    }
}
customElements.define("app-asset", Asset);