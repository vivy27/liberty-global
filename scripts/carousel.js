class Carousel extends HTMLElement {
    constructor() {
        super();
        this.genre = null;
        this.assets = null;
    }

    static get observedAttributes() { return ["genre"]; }

    attributeChangedCallback(name, oldValue, newValue) {
        this.genre = newValue;
    }
    connectedCallback() {
        this.updateRendering();
    }

    updateRendering() {
        this.assets = data.data.reduce((accumulator, item) => {
            accumulator.push(...item.assets);
            return accumulator;
        }, []).filter(asset => asset.genre === this.genre);

        const div = document.createElement('div');
        
        this.assets.forEach(asset => {   
            const assetElement = document.createElement('app-asset');
            assetElement.setAttribute('image', asset.img);
            assetElement.dataset.title = this.title = asset.title;
            div.appendChild(assetElement);
        });
        
        this.appendChild(div);
    }

    static get events() {
        return {
            selectedCarousel: null,
            selectedAsset: null,
            getCarousels(pos) {
                return document.querySelectorAll(`app-carousel:${pos}-child`);
            },
            getAssets(pos) {
                return this.selectedCarousel.querySelectorAll(`app-asset:${pos}-child`);
            },
            moveUp() {
                this.removeAssetSelected();
                if (this.selectedCarousel !== null) {
                    this.selectedCarousel.classList.remove('selected');
                    const prev = this.selectedCarousel.previousElementSibling;
                    if (prev !== null) {
                        this.selectedCarousel = prev;
                        prev.classList.add('selected');
                    }
                    else {
                        this.selectLastCarousel();
                    }

                } else {
                    this.selectLastCarousel();
                }
            },
            moveDown() {
                this.removeAssetSelected();
                if (this.selectedCarousel !== null) {
                    this.selectedCarousel.classList.remove('selected');
                    const next = this.selectedCarousel.nextElementSibling;
                    if (next !== null) {
                        this.selectedCarousel = next;
                        next.classList.add('selected');
                    }
                    else {
                        this.selectFirstCarousel();
                    }
                } else {
                    this.selectFirstCarousel();
                }
            },
            moveNext() {
                if (this.selectedAsset !== null) {
                    this.selectedAsset.classList.remove('selected');
                    const next = this.selectedAsset.nextElementSibling;
                    if (next !== null) {
                        this.selectedAsset = next;
                        next.classList.add('selected');
                    }
                    else {
                        this.selectFirstAsset();
                    }
                } else {
                    this.selectFirstAsset();
                }
            },
            movePrevious() {
                if (this.selectedAsset !== null) {
                    this.selectedAsset.classList.remove('selected');
                    const prev = this.selectedAsset.previousElementSibling;
                    if (prev !== null) {
                        this.selectedAsset = prev;
                        prev.classList.add('selected');
                    }
                    else {
                        this.selectLastAsset();
                    }

                } else {
                    this.selectLastAsset();
                }
            },
            removeAssetSelected() {
                if (this.selectedAsset !== null) {
                    this.selectedAsset.classList.remove('selected');
                    this.selectedAsset = null;
                }
            },
            selectFirstCarousel() {
                const firstElement = this.getCarousels('first')[0];
                this.selectedCarousel = firstElement
                firstElement.classList.add('selected');
            },
            selectLastCarousel() {
                const lastElement = this.getCarousels('last')[0];
                this.selectedCarousel = lastElement
                lastElement.classList.add('selected');
            },
            selectFirstAsset() {
                const firstElement = this.getAssets('first')[0];
                this.selectedAsset = firstElement
                firstElement.classList.add('selected');
            },
            selectLastAsset() {
                const lastElement = this.getAssets('last')[0];
                this.selectedAsset = lastElement
                lastElement.classList.add('selected');
            },
            attach() {
                document.addEventListener('keydown', evt => {
                    evt = evt || window.event;

                    switch (evt.keyCode) {
                        case 37: //Left arrow
                            this.movePrevious();
                            break;
                        case 38: //Up arrow
                            this.moveUp();
                            break;
                        case 39: //Right arrow
                            this.moveNext();
                            break;
                        case 40: //Down arrow
                            this.moveDown();
                            break;
                    }
                });
            }
        }
    }
}

customElements.define("app-carousel", Carousel);

(function () {
    // bind keyboard events to the document
    Carousel.events.attach();
})();